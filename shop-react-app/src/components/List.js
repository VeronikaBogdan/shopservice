import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import moment from 'moment'
import Moment from 'react-moment';
import image from "../img/image.svg";
import "../css/List.css"

const api = new Api.DefaultApi()

class List extends React.Component {

    constructor(props) {
        super(props);
        const id =  this.props.match?.params.id || moment().format('LLLL');
        console.log(id);

        

        this.state = { 
            list: [],
            date: id 
        };

        this.handleReload = this.handleReload.bind(this);
        //this.handleReload();
    }


    async handleReload(event) {
        const response = await api.list({ date: ''/*this.state.targetDate*/ });
        this.setState({ list: response});
    }


    render() {
        return <div>          
            <ul style={{fontSize: ' 25px', color: '#1F8873', listStyle: 'none', display:'flex'}}>
               {this.state.list.map(
                   (list) => 
                     <li style={{fontSize: ' 25px', color: 'black', padding:'40px', paddingBottom:'50px', float:'left', textAlign:'left', flex:'1'}} >
                                             <div style={{color: '#0023A0'}}> 
                                             <div> <img src={image}></img></div>
                                             
        {/*STARS*/}
       
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star"></span>
          <span class="fa fa-star" style={{color: list.color}}></span>
    
        </div>
        <div style={{fontSize: ' 30px', color: '#000000', fontFamily: 'Solway-Bold'}}>"{list.Company}"</div>
        <div  style={{fontFamily: 'Solway',fontSize: ' 20px'}}>{list.Company}, {list.year} </div>
                        <div style={{fontFamily: 'Solway-Light',fontSize: ' 20px' }}> {list.word} </div>
                        <div style={{fontFamily: 'Solway',fontSize: ' 20px'}}>{list.dollar}</div>
                        <div style={{clear:'both'}}></div>
                        <button className="item__button" style={{fontFamily: 'Solway-Bold'}}>Buy</button>
                    </li>)}
                    
             </ul>
             <div style={{clear:'both'}}></div>
            <div style={{padding:'10px'}}> <button className="Button2" style={{fontFamily: 'Pettaya'}} onClick={this.handleReload}>Learn more...</button> </div>
        </div>
        
    }
}

export default withRouter(List);