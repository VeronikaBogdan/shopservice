import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import { Link } from "react-router-dom";
import Moment from 'react-moment';
import moment from 'moment';
import image2 from "../img/image2.svg";
import "../css/Books.css"

const api = new Api.DefaultApi()

class Books extends React.Component {

    constructor(props) {
        super(props);
        const dateParam = this.props.match?.params.id || moment().format('YYYY-MM-DD');
        const parsedDate = moment(dateParam, "YYYY-MM-DD")

        const nearestWeekend = parsedDate.startOf('week').isoWeekday(0);
        const endDate = moment(nearestWeekend).add(6, 'day');

        console.log("Enddate", nearestWeekend.toString(), endDate.toString())

        const startWeek = nearestWeekend.format("YYYY-MM-DD")
        const endWeek = endDate.format("YYYY-MM-DD")



        // TODO reuse data to service from https://citydog.by/post/play-musykanty/
        this.state = {
            books: [],
            start: startWeek,
            end: endWeek

        };

        console.log(this.state.start, this.state.end)
        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


    async handleReload(event) {
        const response = await api.books({ date: this.state.startWeek });
        this.setState({ books: response });
    }

    render() {
        return <div>          
            <ul style={{fontSize: ' 25px', color: '#1F8873', /*bookStyle: 'none',*/ display:'flex', flexDirection: 'column', /*display: 'inline-block', position: 'relative', height: 'auto'*/}}>
               {this.state.books.map(
                   (books) => 
                        <li style={{fontSize: ' 25px', color: 'black',  paddingBottom:'20px',  textAlign:'left', flex:'1'}} >
                            <div class="palka" style={{fontSize: ' 30px', color: '#fff', textAlign:'center'}}>"{books.productName}"</div><br></br>
                            <div style={{paddingLeft: '15px', display: 'flex'}}>
                                <div > <img src={image2}></img></div>                    
                                <div style={{marginLeft: '25px'}}>
                                    <div  style={{fontFamily: 'Solway',fontSize: ' 20px'}}>{books.fName} {books.lName}, {books.year} </div>
                                    {/*STARS*/}                
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star" style={{color: books.color}}></span>      
                                    <div style={{fontFamily: 'Solway-Light',fontSize: ' 20px' }}> {books.sentences} </div>
                                    <div style={{clear:'both'}}></div>
                                    <button className="item__button1" style={{fontFamily: 'Solway-Bold'}}>{books.dollar}</button>
                                </div>
                            </div>
                        </li>)}                   
            </ul>
            <div style={{clear:'both'}}></div>
        </div>
        
    }
}

export default withRouter(Books);