import './App.css';
import React from 'react';
import Books from './components/Books';
import List from './components/List';
import books from "./img/books.svg";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useParams,
  useLocation
} from 'react-router-dom';

function App() {
  return (

    <Router>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>
      <header id="header">
            <div> 
              <Link to={'/'}><img src={books} class="box1" style={{width:'45px',height:'45px'}}></img></Link>
            </div>
            <div class="box2"></div>
            <div class="box3" className="icons"><div class="fa fa-cart-plus"></div></div> 
            <div class="box4"  className="icons" ><div class="fa fa-user-o"></div></div>
        </header>

        {/*    <Link to={'/concerts'}>concerts</Link>      */}

      <div className="App">
        <section>
          <Switch>
            <Route path="/books/language">
              <div class="book-link">
                <ul>
                  <li style={{flex:1, backgroundColor:'#4B4E6D', padding: '30px', color: '#D2D6D9'}}>
                    Language learning
                  </li>
                  <li style={{flex:1}}>
                    <Link to={'/books/school'}>Literature for schoolchildren</Link>
                  </li>
                  <li style={{flex:1}}>
                    Pedagogical literature 
                  </li>
                  <li style={{flex:1}}>
                    Exam preparation, CT
                  </li>
                </ul>
              </div>
              <div class="palka" style={{fontSize: ' 30px', color: '#fff', backgroundColor: '#6D68B1', textAlign: 'left', paddingLeft: '15px', marginBottom: '10px'}}>EDUCATIONAL LITERATURE</div>
              <Books />
            </Route>
            <Route path="/books/school">
              <div class="book-link">
                <ul>
                  <li>
                    <Link to={'/books/language'}>Language learning</Link>
                  </li>
                  <li style={{flex:1, backgroundColor:'#4B4E6D', padding: '30px', color: '#D2D6D9'}}>
                    Literature for schoolchildren
                  </li>
                  <li style={{flex:1}}>
                    Pedagogical literature 
                  </li>
                  <li style={{flex:1}}>
                    Exam preparation, CT
                  </li>
                </ul>
              </div>
              <div class="palka" style={{fontSize: ' 30px', color: '#fff', backgroundColor: '#6D68B1', textAlign: 'left', paddingLeft: '15px', marginBottom: '10px'}}>EDUCATIONAL LITERATURE</div>
              <Books />
            </Route>

            <Route path="/">
               <div id="section-1">
                <nav style={{flex:1}}>
                  <ul className="Nav">
                    <li style={{flex:1, color:'#D2D6D9'}}>
                      <Link to={'/books/language'} style={{color: '#D2D6D9', textDecoration: 'none'}}>Educational literature</Link>
                    </li>
                    <li style={{flex:1,color:'#D2D6D9'}}>
                      <Link to={'/'}  style={{color: '#D2D6D9', textDecoration: 'none'}}>Professional literature</Link>
                    </li>
                    <li style={{flex:1}}>
                      <Link to={'/'} style={{color: '#D2D6D9', textDecoration: 'none'}}>Psychological literature</Link>
                    </li>
                    <li style={{flex:1}}>
                      <Link to={'/'} style={{color: '#D2D6D9', textDecoration: 'none'}}>Fantasy and science fiction</Link>
                    </li>
                    <li style={{flex:1}}>
                      <Link to={'/'} style={{color: '#D2D6D9', textDecoration: 'none'}}>Detectives </Link>
                    </li>
                    <li style={{flex:1}}>
                      <Link to={'/'} style={{color: '#D2D6D9', textDecoration: 'none'}}>Exclusive products</Link>
                    </li>
                    <li style={{flex:1}}>
                      <Link to={'/'} style={{color: '#D2D6D9', textDecoration: 'none'}}>Popular products</Link>
                    </li>
                    <li style={{flex:1}}>
                      <Link to={'/'} style={{color: '#D2D6D9', textDecoration: 'none'}}>Books with autographs by authors</Link>
                    </li>
                  </ul>
                </nav>
                <div style={{flex:3}}>
                  <button className="item__button" style={{fontFamily: 'Solway-Bold', margin: '400px 100px 30px 700px'}}>Scroll</button>
                </div>
              </div>
              <div class="palka" style={{fontSize: ' 30px', color: '#fff'}}>FANTASY</div>
              <List />
              <div class="palka" style={{fontSize: ' 30px', color: '#fff'}}>DETECTIVE</div>
              <List />
              <div class="palka" style={{fontSize: ' 30px', color: '#fff'}}>ROMANTIC</div>
              <List />
              <div class="palka" style={{fontSize: ' 30px', color: '#fff'}}>EDUCATIONAL LITERATURE</div>
              <List />
            </Route>
            <Route path="*">
              <NoMatch />
            </Route>
          </Switch>
          <Route path="/language">
              <Redirect to="/books/language" />
          </Route>
        </section>      
      </div> 

     <footer style={{fontFamily: 'Teko-Light', fontSize: '22px'}}>
        <div id="name">
          <ul class="footer_left">
            <li>FnBook.shop@gmail.com</li>
            <li>
              <h5><b>&copy;FnBook, 2021</b></h5>
            </li>
            <li></li>
          </ul>
        </div>
        <div class="list">
          <ul>
            <li>Delivery</li>
            <li>Payment</li>
          </ul>
        </div>
        <div class="list">
          <ul>
            <li>News FnBook</li>
            <li>Vacancies</li>
          </ul>
        </div>
     </footer>

    </Router>
  );
}

function NoMatch() {
  let location = useLocation();

  return (
    <div>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  );
}

export default App;
